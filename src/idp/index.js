const { db } = require("../db");

const getIdpID = async ({ req }) => {
  const user_id = req.headers["idp_sub"];

  const errorResult = {
    user_id: null,
    doctor_id: null,
    status: false,
  };

  if (!user_id) {
    return errorResult;
  }

  const userQuery = {
    sql: `SELECT id_pasien as id FROM pasien WHERE idp_id = @user_id`,
  };

  const doctorQuery = {
    sql: `SELECT id_dokter as id FROM dokter WHERE idp_id = @user_id`,
  };

  try {
    const users = await db.run({
      ...userQuery,
      params: {
        user_id,
      },
    });

    const doctors = await db.run({
      ...doctorQuery,
      params: {
        user_id,
      },
    });

    const user = users[0].map((row) => row.toJSON());
    const doctor = doctors[0].map((row) => row.toJSON());

    return {
      user_id: user[0]?.id,
      doctor_id: doctor[0]?.id,
      status: Boolean(user.length) || Boolean(doctor.length),
    };
  } catch (error) {
    return errorResult;
  }
};

module.exports = getIdpID;
