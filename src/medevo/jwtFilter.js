const getIdpID = require("../idp");

const JwtFilter = async (req, res, next) => {
  const jwt = req.header("x-apigateway-api-userinfo");
  const { user_id, doctor_id, status } = await getIdpID({ req });

  if (status) {
    req.app.locals.dokterId = doctor_id;
    req.app.locals.pasienId = user_id;
    req.app.locals.userId = user_id;
    next();
  } else if (jwt) {
    let jsonBuff = Buffer.from(jwt, "base64").toString("ascii");
    const buff = JSON.parse(jsonBuff);

    if (buff.id && buff.id != "" && buff.id != null) {
      req.app.locals.dokterId = buff.id;
      req.app.locals.pasienId = buff.id;
      req.app.locals.userId = buff.id;
      next();
    } else {
      return res.status(401).send({
        status: 401,
        msg: "Token is not valid",
      });
    }
  } else {
    return res.status(401).send({
      status: 401,
      msg: "Token is missing",
    });
  }
};
module.exports = JwtFilter;
