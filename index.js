module.exports = {
  db: require("./src/db"),
  idp: require("./src/idp"),
  medevo: require('./src/medevo'),
};
